(function () {
    'use strict';

    angular.module('app.warehouse')
        .controller('customwarehouseController', ['$scope', '$http', customwarehouseController])

    function customwarehouseController($scope, $http) {

            $http.get("localhost:7070/wms/warehouse/list")
           .success(function (response) 
            {
                $scope.value = response;
            });

    }

})(); 